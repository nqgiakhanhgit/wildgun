﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GroundCheck : MonoBehaviour
{
    

    public bool IsGround( bool isGround,Transform feetPos, float checkRadius, LayerMask whatisground)
    {
        isGround = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatisground);
        return isGround;
    }


}
