﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerController : MonoBehaviour
{

    private MoveCotroller move;
    private Animator PlayerAnimator;
    [SerializeField]
    private float speed;
    [SerializeField]
    private bool jump;
    [SerializeField]
    private float vertical;
    [SerializeField]
    private float horizontal;
    [SerializeField] private float _jumpValue;

    // Start is called before the first frame update
    void Start()
    {

        PlayerAnimator = GetComponentInChildren<Animator>();
        move = GetComponent<MoveCotroller>();
    }
    
   
    // Update is called once per frame
    void Update()
    {
      
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);
        //move
        move.Move(horizontal, speed,PlayerAnimator);
        //Flip
        move.Flip(horizontal);

        //Jump
        move.Jump(_jumpValue, jump,PlayerAnimator);

    }
}
