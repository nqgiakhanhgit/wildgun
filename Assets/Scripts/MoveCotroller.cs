﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MoveCotroller : MonoBehaviour
{
    [SerializeField]
    private LayerMask whatisground;
    private BoxCollider2D boxcollider2d;
    private Rigidbody2D rb;
    public bool facingRight;
    Vector3 localScale;
    public bool isGround;
    public Transform feetPos;
    private GroundCheck isground;
    [SerializeField] private float checkRadius;
 
    
    private void Start()
    {
        boxcollider2d = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        localScale = transform.localScale;
        isground = GetComponent<GroundCheck>();

    }
    public void Move(float hori, float speed,Animator Player)
    {
        transform.position += new Vector3(hori, 0, 0) * speed * Time.deltaTime;
        if(hori!=0)
        Player.SetTrigger("Run");
    }
    public void Flip(float hori)
    {
        if (hori > 0)
        {
            facingRight = true;
        }
        else
        if (hori< 0)
        {
            facingRight = false;
        }
        if(facingRight  && localScale.x <0 || !facingRight && localScale.x > 0)
        {
            localScale.x = localScale.x * -1;
        }
        transform.localScale = localScale;
    }
    public void Jump(float jumpvalue, bool input,Animator Player)
    {
        if(isground.IsGround(isGround, feetPos,checkRadius,whatisground)==true && input == true)
        {
            rb.velocity = Vector2.up * jumpvalue;

            Player.SetTrigger("Jump");
        }
       
    }


 
    
}
